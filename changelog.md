V 1.2.0

Improvements:
- Generally renaming base_filters to parameters, and better describe them as request parameter identifiers
- Updated readme accordingly
- Added missing views_display_extender config schema
- Added first functional tests including a test module with test-view

Other/fixes:
- Drupal 11 compatibility
- Unsetting unecessary help_text form value
- Added missing drupal:views dependency
- Fixed config storage keys + Updatehook to migrate existent ones

There is an updatehook included so please run updb/update.php to migrate from 1.1.x

V 1.1.0

Features:

- New option "case-insensitive/case-sensitive" comparison.
- Copy basefilters from other displays if available.
- Show the configuration part of README inside usuage tab.

Other/fixes:
- Added helptext for base filters.
- Stop looking for combinations if no direct match.
- lt and gt expression-operators cast actual numbers if both values are numeric.
- Improved the label regex.
- Early return if no base_filters are configurated.
