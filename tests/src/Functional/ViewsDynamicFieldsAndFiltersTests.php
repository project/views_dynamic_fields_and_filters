<?php

namespace Drupal\Tests\views_dynamic_fields_and_filters\Functional;

use Drupal\Tests\views\Functional\ViewTestBase;

/**
 * Testing base functionality of this module.
 *
 * @group views_dynamic_fields_and_filters
 */
class ViewsDynamicFieldsAndFiltersTests extends ViewTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'olivero';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'node',
    'views',
    'views_test_config',
    'views_ui',
    'views_dynamic_fields_and_filters',
    'vdff_tests',
  ];

  /**
   * {@inheritdoc}
   */
  public static $testViews = [
    'vdff_test_content',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['views_test_config']): void {
    parent::setUp($import_test_views, ['vdff_tests']);
    $account = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($account);
  }

  /**
   * Assert that the module installed correctly.
   */
  public function testModuleInstalls() {
    // If we get here, then the module was successfully installed.
    $this->assertTrue(TRUE, 'Module installed correctly.');
  }

  /**
   * Tests the vdff_test_content view including its schema.
   */
  public function testDynamicFieldsAndFilters() {

    $this->drupalGet('vdff_test/vdff-test-data');
    // Check if the vdff_test_content page is responding.
    $this->assertSession()->statusCodeEquals(200);

    // Check if the published filter is not rendered.
    $this->assertSession()->responseNotContains('form-item-status');
    // Check if the title field is not rendered.
    $this->assertSession()->responseNotContains('views-field-title');

    $this->drupalGet('vdff_test/vdff-test-data', ["query" => ["type" => "page"]]);
    // Assert the published filter is rendered.
    $this->assertSession()->responseContains('form-item-status');
    // Check if the title field is rendered.
    $this->assertSession()->responseContains('views-field-title');

  }

  /**
   * Test dff-labels.
   */
  public function testDffLabels() {
    $test_view = \Drupal::entityTypeManager()
      ->getStorage('view')
      ->load('vdff_test_content')
      ->getExecutable();
    $display = $test_view->getDisplay();
    $extenders = $display->getExtenders();
    $dff = $extenders['views_dynamic_fields_and_filters'];

    $labels_that_should_work = [
      "dff3|foobar| Test",
      "dff3|foobar|",
      "dff2|{gt:5}|AND|dff4|{in:foo,bar}| Test",
      "dff2|{cn:5dsa}|AND|dff4|{in:foo,bar}|OR|dff3|page| Test",
    ];

    foreach ($labels_that_should_work as $label) {
      $this->assertTrue($dff->isDffLabel($label), 'Label recognized as dff label');
    }

    $labels_that_should_not_work = [
      "dff3|foobar",
      "|dff3|foobar|",
    ];

    foreach ($labels_that_should_not_work as $label) {
      $this->assertFalse($dff->isDffLabel($label), 'Label recognized as dff label');
    }

  }

}
