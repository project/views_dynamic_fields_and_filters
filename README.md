# Views dynamic fields and filters

###### Enables site builders to easily enable or disable fields and filters in a view conditionally based on the values of exposed/contextual filters or basically any request parameters.

Can be used for any display type and any format that uses the fields defined in the view display.

### Idea

The initial idea was to build more convenient content browsers where you show specific fields and filters only if the relevant content type was selected without having to programm custom code.

With this resulting module there are endless use cases to apply this. In example:
- You have a JSON REST export made with views and wanted to show specific fields or apply a specific filter only if specific query parameters and values are supplied.
- You have a search page made with views and you wanted to show a "range filter" only if the user selected "search mode" to be "range" or show specific fields only if a user checked a checkbox "show details".
- Or express even more complex "funnels" i.e: Apply non-exposed filter "a" only if exposed filter "b" is one of "foo" or "bar" and exposed filter "c" is greater than 25

### Configuration:
##### General
When installing this module, a new section **Dynamic fields and filters** will be available at the advanced column on each display in the views edit-UI.
The Settings are per display, if you already have settings in another display of the same view you can copy them into the current display with a button in the dialog.


##### Configurate "parameters"
In the dialog you can assign up to 9 request parameters to listen to, so that you can  enable or disable fields or filters when the value of a parameter matches a certain condition.
Enter the request parameter names as they appear in the query i.e "type" for view-url "example.com/my_view?type=foobar". Works for POST-parameters as well. You can also find the parameter names of exposed filters or exposed sorts in their edit-dialog under "filter identifier" or "sort field identifier". For Contextual filters it will only work if their "provide a default value type" is "Query parameter.


### Adjust "Administrative titles" to enable or disable fields and filters.
##### Title syntax
Within the "administrative title" of a field or a filter you can use this syntax to only enable them if the value of a configurated parameter matches the given expression.
You can refer to the values of your parameters with `dff1` to `dff9`.

The syntax is:

`dff{1..9}|expression|"Your custom Administrative title"`

Where `dff1` to `dff9`  refers to the value of a parameter.

`expression` is either the string value to match or a more specific expression.

And `"Your custom Administrative title"` is ...your custom administrative title. You could also leave this blank, but you must always add to trailing pipe after "expression"


##### Expression syntax
If you just want to loosely compare strings (like `'abc' == 'abc'` in php or js)
you only write the string value, in example:

`dff3|foobar|` expresses "Value of parameter3 matches 'foobar'"


For all other expressions you write:

`|{operator:value}|`

Available value operators are:

`{neq:value}` - Not equals

`{in:value,value2,value3}` - In array (delimiter is "," without a space)

`{nin:value,value2,value3}` - Not in array

`{gt:value}` - Greater than

`{lt:value}` - Smaller than

`{cn:value}` - Contains

`{ncn:value}` - Not contains

Examples:
`dff2|{gt:5}|` expresses "Value of parameter2 is greater than 5"
`dff4|{in:foo,bar}|` expresses "Value of parameter4 is either "foo" or "bar""

If the query value itself is an array like in this example query:
`?types[]=foo&types[]=bar`
Each element will be evaluated, the expression is true if one the elements matches.

Always enter the url-decoded values, the value of foo in the query `?foo=bar+foobar+barfoo` would be `bar foobar barfoo`

There is also a setting "Case insensitive string comparison." in the settings dialog in case you want that.

##### Combine expressions
You can logically chain multiple conditions with the chaining syntax:

`dff{1..9}|expression|OPERATOR|dff{1..9}|expression|"Your custom Administrative title"`

Where `OPERATOR` is either `AND`, `OR` or `XOR` (uppercase), you can use a total of 10 operators per label, each must be followed by the `"|dff{1..9}|expression|"` pattern.

In example:

`dff2|{gt:5}|AND|dff4|{in:foo,bar}|OR|dff3|foobar|` *(dff2 is greater than 5 AND dff4 is one of "foo" or "bar") OR dff3 matches "foobar"*


*For detailed information on how they work, check out the php´s [Logical Operators](https://www.php.net/manual/en/language.operators.logical.php) The here provided operators work with the precedence like using "&&", "||" and "xor"*

You can use the condition syntax for the parameters as well, this way you can make even more complex chaining without writing long and repeating conditions.

##### In depth example: Show field "Author" only if exposed "Content type" filter is "Basic Page"
Lookup the "filter identifier" of your exposed "content type" filter, i.e `type` and enter it in this Dynamic fields and filters settings in the advanced tab in the tab "Base filters" i.e as `dff1`, then you lookup the value that is used when "Basic Page" is selected, i.e `page`. klick on the field "Author" and go to the tab "Administrative title" and enter:

`dff1|page| "Your custom Administrative title"`

### Installation
Install as you would normally install a Drupal module.

In example:
`composer require drupal/views_dynamic_fields_and_filters`

### Requirements

This module works out of the box on a minimal Drupal install as it only requires "Views" which is required by system.

##### Drupal Core:

Tested with core 9.3.x, 9.4.x, 10.0.x 10.1.x and php 8.1.0 & 7.4.26

Tested with core modules "Views REST export" & "Serialization" (They are not required to be enabled)

##### Contrib:

Compatible and tested with "Views Conditional"


