<?php

namespace Drupal\views_dynamic_fields_and_filters\Plugin\views\display_extender;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;

/**
 * DynamicFieldsAndFilters display extender plugin.
 *
 * @ingroup views_display_extender_plugins
 *
 * @ViewsDisplayExtender(
 *   id = "views_dynamic_fields_and_filters",
 *   title = @Translation("Dynamic fields and filters"),
 *   help = @Translation("Dynamically show and apply fields and filters based on an exposed filter."),
 *   no_ui = FALSE
 * )
 */
class DynamicFieldsAndFilters extends DisplayExtenderPluginBase {

  /**
   * {@inheritdoc}
   *
   * Provide the default summary for options and category in the views UI.
   */
  public function optionsSummary(&$categories, &$options) {
    $categories['views_dynamic_fields_and_filters'] = [
      'title' => t('Dynamic fields and filters'),
      'column' => 'third',
    ];

    $options['views_dynamic_fields_and_filters'] = [
      'category' => 'views_dynamic_fields_and_filters',
      'title' => t('Parameters'),
      'value' => $this->getParametersFormatted(),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Provide a form to edit options for this plugin.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    if ($form_state->get('section') == 'views_dynamic_fields_and_filters' && $form_state) {

      $form['#title'] .= t('Dynamic fields and filters for this display');
      $form['views_dynamic_fields_and_filters']['#type'] = 'container';
      $form['views_dynamic_fields_and_filters']['tabs'] = [
        '#type' => 'vertical_tabs',
        '#default_tab' => 'edit-fieldset',
      ];

      // The parameters tab.
      $form['views_dynamic_fields_and_filters']['parameters'] = [
        '#type' => 'details',
        '#title' => $this->t('Parameters'),
        '#group' => 'tabs',
      ];
      $form['views_dynamic_fields_and_filters']['parameters']['help_text'] = [
        '#type' => 'item',
        '#title' => $this->t('Parameters'),
        '#description' => $this->t('Assign up to 9 request parameters to listen to, so that you can show or hide fields and filters based on their value. In the administrative titles you can refer to them as "dff1" to "dff9". Here you need to enter the request parameter names as they appear in the query i.e "type" for view-url "example.com/my_view?type=foobar". Also, you find the parameter names of exposed filters or exposed sorts in their edit-dialog under "filter identifier" or "sort field identifier". For Contextual filters it will only work if their "provide a default value type" is "Query parameter"'),
      ];
      $form['views_dynamic_fields_and_filters']['parameters']['#tree'] = TRUE;

      $filters = $this->getParameters();
      for ($i = 1; $i < 10; $i++) {
        $form['views_dynamic_fields_and_filters']['parameters']['dff' . $i] = [
          '#title' => 'dff' . $i,
          '#type' => 'textfield',
          '#placeholder' => $this->t('None'),
          '#default_value' => !empty($filters['dff' . $i]) ? $filters['dff' . $i] : '',
        ];
      }

      // The settings tab.
      $form['views_dynamic_fields_and_filters']['settings_wrap'] = [
        '#type' => 'details',
        '#title' => $this->t('Settings'),
        '#group' => 'tabs',
      ];

      $form['views_dynamic_fields_and_filters']['settings_wrap']['case_insensitive'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Case insensitive string comparison.'),
        '#description' => $this->t('Format values to lowercase before comparing'),
        '#default_value' => !empty($this->options['settings']['case_insensitive']) ? $this->options['settings']['case_insensitive'] : 'off',
      ];

      $form['views_dynamic_fields_and_filters']['settings_wrap']['add_query_cache_tags'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Add query_args cache context to view (if view cache enabled).'),
        '#description' => $this->t('Some formats i.e "Serializer"/"Rss-feed" do not know their response changes if fields are enabled/disabled with dff. By adding the url.query_args context ensure that for each response variation caused by query, there is a seperate cache entry.'),
        '#default_value' => !empty($this->options['settings']['add_query_cache_tags']) ? $this->options['settings']['add_query_cache_tags'] : 'off',
      ];

      // If available, show "copy from other display".
      $displays_with_dff_config = [];
      foreach ($form_state->get('view')->get('display') as $display_id => $display) {
        $extenders = $display['display_options']['display_extenders'];
        $has_config = isset($extenders['views_dynamic_fields_and_filters']) && !empty($extenders['views_dynamic_fields_and_filters']);
        if ($form_state->get('display_id') != $display_id && $has_config) {
          $displays_with_dff_config[$display_id] = $display['display_title'] . ' (' . $display_id . ')';
        }
      }
      if (count($displays_with_dff_config) > 0) {

        $displays_with_dff_config['do_nothing'] = $this->t('No selection');
        $form['views_dynamic_fields_and_filters']['settings_wrap']['copy_from_display'] = [
          '#type' => 'select',
          '#title' => $this->t('Copy parameters and settings from other display'),
          '#description' => $this->t('Warning, if you make a selection, all your configuration from this display will be overwritten! Select the display to copy from, click on apply/save to copy the values.'),
          '#options' => $displays_with_dff_config,
          '#default_value' => 'do_nothing',
        ];
      }
      else {
        $form['views_dynamic_fields_and_filters']['settings_wrap']['copy_from_display_no_display'] = [
          '#type' => 'select',
          '#title' => $this->t('Copy parameters and settings from other display'),
          '#options' => ['No configuration found in other displays of this view!'],
          '#default_value' => 'do_nothing',
        ];
      }

      // The usuage tab.
      $form['views_dynamic_fields_and_filters']['description'] = [
        '#type' => 'details',
        '#title' => $this->t('Usuage'),
        '#group' => 'tabs',
      ];

      // Display configuration section of README.md (html is manually created).
      $form['#attached']['library'][] = 'views_dynamic_fields_and_filters/readme';
      $module_path = \Drupal::service('module_handler')->getModule('views_dynamic_fields_and_filters')->getPath();
      $readme_path = \Drupal::service('file_system')->realpath($module_path . '/README.html');
      $form['views_dynamic_fields_and_filters']['description']['usuage_2'] = [
        '#markup' => file_get_contents($readme_path),
      ];

    }
  }

  /**
   * {@inheritdoc}
   *
   * Validate the options form.
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) {
    if ($form_state->get('section') == 'views_dynamic_fields_and_filters') {
      $parameters = $form_state->getValue('parameters');
      foreach ($parameters as $key => $filter) {
        if (preg_match('/\s/', $filter)) {
          $el = $form['views_dynamic_fields_and_filters']['parameters'][$key];
          $form_state->setError($el, $this->t("A request parameter name may not contain whitespace!"));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   * Handle any special handling on the validate form.
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    if ($form_state->get('section') == 'views_dynamic_fields_and_filters') {

      $parameters = $form_state->getValue('parameters');
      $copy_from_display = $form_state->getValue('copy_from_display');
      $case_insensitive = $form_state->getValue('case_insensitive');
      $add_query_cache_tags = $form_state->getValue('add_query_cache_tags');

      // Copy options from other display.
      if ($copy_from_display && $copy_from_display != 'do_nothing') {
        if ($form_state->get('view')->getDisplay($copy_from_display)) {
          $ext = $form_state->get('view')->getDisplay($copy_from_display)['display_options']['display_extenders'];
          $opts = $ext['views_dynamic_fields_and_filters'];
          $this->options = $opts;
          return;
        }
      }

      // Unset the help_text...
      unset($parameters['help_text']);
      $this->options['parameters'] = $parameters;
      $this->options['settings']['case_insensitive'] = $case_insensitive;
      $this->options['settings']['add_query_cache_tags'] = $add_query_cache_tags;

    }
  }

  /**
   * Get the parameters configuration for this display.
   *
   * @return array
   *   The parameters configuration for this display.
   */
  public function getParameters() {
    if (!empty($this->options['parameters'])) {
      return $this->options['parameters'];
    }
    return [];
  }

  /**
   * Format filters to a string to be shown in the optionsSummary.
   *
   * @return string
   *   The formatted filters.
   */
  public function getParametersFormatted() {
    $parameters = array_filter($this->getParameters());
    if (!empty($parameters)) {
      return http_build_query($parameters, "", ", ");
    }
    return t('None');
  }

  /**
   * Return the configurated filters and their selected values.
   *
   * @return array
   *   The parameters with the values from current request.
   */
  public function getParametersWithValues() {
    $parameters = $this->getParameters();
    $parameters_with_values = [];
    foreach ($parameters as $key => $filter_name) {
      if (!empty($filter_name)) {
        $value = $this->view->getRequest()->get($filter_name);
        $parameters_with_values[$key] = [
          "name" => $filter_name,
          "value" => $value,
        ];
      }
    }
    return $parameters_with_values;
  }

  /**
   * Compare supplied value with supplied condition.
   *
   * @param string|array $value
   *   The value(s) to evaluate the condition on.
   * @param string $condition
   *   Either the string to match or a more detailed expression.
   *
   * @return bool
   *   The test result.
   */
  public function evaluateCondition($value, $condition) {

    // If value is an array return true if one of entries matches.
    if (is_array($value)) {
      $result = FALSE;
      foreach ($value as $v) {
        if (!$result) {
          $result = $this->evaluateCondition($v, $condition);
        }
      }
      return $result;
    }

    $expression = FALSE;
    $case_insensitive = $this->options['settings']['case_insensitive'] ?? FALSE;
    $value = $case_insensitive ? mb_strtolower($value) : $value;

    // Check for value expressions (enclosed in braces).
    if (preg_match('/^{(.*)}$/', $condition, $matches)) {
      $expression = isset($matches[1]) ? explode(':', $matches[1], 2) : FALSE;

      // No "expression:value" pattern found.
      if ($expression && count($expression) != 2) {
        $expression = FALSE;
      }
      // Expression not recognized.
      if ($expression && !preg_match('/^(neq|in|nin|gt|lt|cn|ncn)$/', $expression[0])) {
        $expression = FALSE;
      }
    }

    // No expression syntax found, loosely compare.
    if (!$expression) {
      $match_value = $case_insensitive ? mb_strtolower($condition) : $condition;
      return $value == $match_value;
    }

    // Evaluate the expression.
    $match_value = $case_insensitive ? mb_strtolower($expression[1]) : $expression[1];
    switch ($expression[0]) {
      case 'neq':
        return $value != $match_value;

      case 'in':
        return in_array($value, explode(',', $match_value));

      case 'nin':
        return !in_array($value, explode(',', $match_value));

      case 'gt':
        if (is_numeric($value) && is_numeric($match_value)) {
          $value = +$value;
          $match_value = +$match_value;
        }
        return $value > $match_value;

      case 'lt':
        if (is_numeric($value) && is_numeric($match_value)) {
          $value = +$value;
          $match_value = +$match_value;
        }
        return $value < $match_value;

      case 'cn':
        return strpos($value, $match_value) !== FALSE;

      case 'ncn':
        return strpos($value, $match_value) === FALSE;

    }
  }

  /**
   * Extracts conditions from a string and evaluates them.
   *
   * Returns true if not a dff label or test positive.
   *
   * @param string $admin_label
   *   The string to extract the conditions from.
   *
   * @return bool
   *   The test result.
   */
  public function testLabel($admin_label) {

    // Check if at least one "basefilter|condition|" pattern exists.
    if (!$this->isDffLabel($admin_label)) {
      return TRUE;
    }
    $parameters = $this->getParametersWithValues();
    $split = explode("|", $admin_label);
    $filter1 = $split[0];
    $condition1 = $split[1];

    // |operator|basefilter|condition| Each combination consists of 3 parts.
    // Support up to 10 combinations.
    $combinations = [];
    for ($i = 2; $i <= 32; $i++) {

      // Check if 1st part is operator.
      if (!empty($split[$i]) && preg_match('/^(AND|OR|XOR)$/', $split[$i])) {

        // Check if 2nd is basefilter and 3rd is condition.
        if (!empty($split[$i + 1]) && preg_match('/^dff([1-9]){1}$/', $split[$i + 1]) && isset($split[$i + 2])) {
          $operator = $split[$i];
          $filterx = $split[$i + 1];
          $conditionx = $split[$i + 2];
          $resultx = FALSE;
          if (array_key_exists($filterx, $parameters)) {
            $resultx = $this->evaluateCondition($parameters[$filterx]['value'], $conditionx);
          }
          // Store the results.
          $combinations[] = [
            "operator" => $operator,
            "result" => $resultx,
          ];
        }
      }
      else {
        break;
      }
    }

    $result = FALSE;
    if (array_key_exists($filter1, $parameters)) {
      $result = $this->evaluateCondition($parameters[$filter1]['value'], $condition1);
    }

    if (empty($combinations)) {
      return $result;
    }

    // Combine results.
    foreach ($combinations as $combination) {
      switch ($combination['operator']) {
        case 'AND':
          $result = $result && $combination['result'];
          break;

        case 'OR':
          $result = $result || $combination['result'];
          break;

        case 'XOR':
          $result = $result xor $combination['result'];
          break;
      }
    }

    return $result;
  }

  /**
   * Check whether a given label matches dff pattern.
   *
   * @param string $admin_label
   *   The string e.g the "Administrative title" to extract the conditions from.
   *
   * @return bool
   *   The test result.
   */
  public function isDffLabel($admin_label) {
    return preg_match('/^dff([1-9]){1}[|]/', $admin_label) && count(explode("|", $admin_label)) > 2;
  }

  /**
   * Add query_args cache context if cache not disabled.
   *
   * Some formats i.e "Serializer"/"Rss-feed" do not know their
   * response changes if fields are enabled/disabled with dff.
   * By adding the url.query_args context we ensure that for each
   * response variation caused by query, there is a seperate cache entry.
   */
  public function extendCacheIfEnabled() {

    if (!isset($this->options['settings']['add_query_cache_tags'])) {
      return;
    }
    $opts = $this->view->getDisplay()->options;
    if (!$opts['defaults']['cache'] || $opts['cache']['type'] == 'none') {
      return;
    }

    $this->view->addCacheContext('url.query_args');
  }

}
